import numpy
import matplotlib.pyplot as plt

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.utils import np_utils

# Define baseline model
def baseline_model():

    # Create model
    model = Sequential()
    model.add(Dense(num_pixels, input_dim=num_pixels, kernel_initializer='normal', activation='relu'))
    model.add(Dense(num_classes, kernel_initializer='normal', activation='softmax'))

    # Compile model
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model

# Fix random seed for reproductivity
seed = 7
numpy.random.seed(seed)

# Load MNIST data set
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# Flatten 28*28 image as a 784 vector for all data set
print(f'\n>>> Reshaping ...')
print(f'x_train shape {x_train.shape}')
print(f'y_train shape {y_train.shape}')
print(f'x_test shape {x_test.shape}')
print(f'y_test shape {x_test.shape}')

num_pixels = x_train.shape[1] * x_train.shape[2]
x_train = x_train.reshape(x_train.shape[0], num_pixels).astype('float32')
x_test = x_test.reshape(x_test.shape[0], num_pixels).astype('float32')

print(f'\n<<< Reshaping done')
print(f'x_train shape {x_train.shape}')
print(f'y_train shape {y_train.shape}')
print(f'x_test shape {x_test.shape}')
print(f'y_test shape {x_test.shape}')

# Normalize inputs from 0-255 to 0-1
print(f'\n>>> Normalizing inputs ...')
print(f'x_train shape {x_train.shape}')
print(f'x_test shape {x_test.shape}')

x_train = x_train / 255
x_test = x_test / 255

print(f'\n<<< Normalizing inputs done')
print(f'x_train shape {x_train.shape}')
print(f'x_test shape {x_test.shape}')

# One hot encoding
print(f'\n>>> One hot encoding ...')
print(f'y_train shape {y_train.shape}')
print(f'y_test shape {y_test.shape}')

y_train = np_utils.to_categorical(y_train)
y_test = np_utils.to_categorical(y_test)
num_classes = y_test.shape[1]

print(f'\n<<< One hot encoding done')
print(f'y_train shape {y_train.shape}')
print(f'y_test shape {y_test.shape}')


# build the model
model = baseline_model()

# fit the model
model.fit(x_train, y_train, validation_data=(x_test, y_test), epochs=10, batch_size=200, verbose=2)

# final evaluation of the model
scores = model.evaluate(x_test, y_test, verbose=0)
print('Baseline error : %.2f%%' % (100 - scores[1] * 100))